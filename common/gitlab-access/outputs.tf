output "argocd_token" {
  value     = gitlab_deploy_token.argocd.token
  sensitive = true
}

output "runners_token" {
  value = data.gitlab_group.www.runners_token
}
