variable "group" {
  type    = string
  default = "shivjm-www"
}

variable "username" {
  type    = string
  default = "argocd"
}

variable "argocd_webhook_url" {
  type = string
}

variable "argocd_webhook_secret" {
  type      = string
  sensitive = true
}
