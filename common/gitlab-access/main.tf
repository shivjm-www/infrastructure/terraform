resource "gitlab_deploy_token" "argocd" {
  group    = var.group
  name     = "Argo CD"
  username = var.username
  scopes   = ["read_repository"]
}

data "gitlab_group" "www" {
  full_path = var.group
}

resource "gitlab_group_hook" "argocd" {
  group                   = "shivjm-www/infrastructure"
  url                     = var.argocd_webhook_url
  token                   = var.argocd_webhook_secret
  enable_ssl_verification = false
  push_events             = true
  subgroup_events         = true
}
