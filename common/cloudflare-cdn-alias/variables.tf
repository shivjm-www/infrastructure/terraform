variable "cloudflare_zone_id" {
  type      = string
  sensitive = true
}

variable "cloudflare_zone" {
  type        = string
  description = "Used for matching URLs."
}

variable "record_name" {
  type = string
}

variable "new_subdomain" {
  type      = string
  sensitive = true
}

variable "target_domain" {
  type      = string
  sensitive = true
}

variable "bucket_name" {
  type = string
}
