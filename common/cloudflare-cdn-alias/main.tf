resource "cloudflare_record" "cname" {
  zone_id = var.cloudflare_zone_id
  name    = var.new_subdomain
  type    = "CNAME"
  value   = var.target_domain
  proxied = true
}

resource "cloudflare_page_rule" "allow_bucket_access" {
  zone_id  = var.cloudflare_zone_id
  target   = "${var.new_subdomain}.${var.cloudflare_zone}/file/${var.bucket_name}/*"
  priority = 2

  actions {
    cache_level = "simplified"
  }
}

resource "cloudflare_page_rule" "prevent_other_access" {
  zone_id  = var.cloudflare_zone_id
  target   = "${var.new_subdomain}.${var.cloudflare_zone}/file/*/*"
  priority = 1

  actions {
    forwarding_url {
      url         = "https://secure.backblaze.com/404notfound"
      status_code = 302
    }
  }
}
