locals {
  application = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "ApplicationSet"
    metadata = {
      name      = var.application_name
      namespace = "argocd"
      finalizers = [
        "resources-finalizer.argocd.argoproj.io"
      ]
    },
    spec = {
      generators = [
        {
          clusters = {
            selector = {
              matchLabels = {
                "cluster-type" = var.target_cluster_type,
              }
            }
          }
        }
      ],
      template = {
        metadata = {
          name = var.application_name
          annotations = {
            for event in [
              "on-sync-succeeded",
              "on-health-degraded",
              "on-sync-failed",
              "on-sync-running",
              "on-sync-status-unknown"
            ] : "notifications.argoproj.io/subscribe.${event}.sjmDiscord" => ""
          }
        }
        spec = {
          project = "default",
          source = {
            repoURL        = var.application_repository,
            targetRevision = var.application_revision,
            path           = var.application_repository_path,
            directory = {
              jsonnet = {
                tlas = concat(
                  [
                    for v in var.tlas : {
                      # It would be better to only use `code = true` for
                      # non-strings, but `type` only works in the
                      # console, so there’s no way to tell.
                      name  = v[0],
                      value = jsonencode(v[1]),
                      code  = true,
                    }
                  ],
                  [
                    { name = "kubernetesServer", value = var.destination_cluster == null ? "https://kubernetes.default.svc" : var.destination_cluster },
                    { name = "namePrefix", value = var.name_prefix }
                  ]
                ),
                libs = ["vendor", "libs"]
              },
              recurse = false,
            }
          },
          syncPolicy = {
            automated = {
              prune      = true,
              allowEmpty = false,
            }
          },
          syncOptions = [
            "CreateNamespace=false",
            "PruneLast=true"
          ],
          destination = {
            namespace = "argocd",
            server    = "{{server}}",
          }
        }
      }
    }
  }
}

resource "kubectl_manifest" "apps" {
  yaml_body = yamlencode(local.application)
}
