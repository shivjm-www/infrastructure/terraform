variable "application_name" {
  type = string
}

variable "target_cluster_type" {
  type = string
}

variable "name_prefix" {
  type        = string
  description = "Prefix to use when naming Application resources."
}

variable "application_repository" {
  type = string
}

variable "application_revision" {
  type = string
}

variable "application_repository_path" {
  type = string
}

variable "destination_cluster" {
  type      = string
  default   = null
  sensitive = true
}

variable "tlas" {
  type      = any
  sensitive = true
}
