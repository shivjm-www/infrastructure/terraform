output "bastion_ip" {
  value = digitalocean_droplet.bastion.ipv4_address
}

output "bastion_port" {
  value = var.ssh_port
}
