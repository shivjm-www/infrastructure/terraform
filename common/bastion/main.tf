resource "digitalocean_ssh_key" "bastion" {
  name       = var.ssh_key_name
  public_key = file(var.ssh_key_file)
}

resource "digitalocean_droplet" "bastion" {
  image         = var.node_image
  name          = var.node_name
  region        = var.do_region
  size          = var.node_size
  backups       = false
  tags          = concat(var.tags, [])
  vpc_uuid      = var.main_private_network_uuid
  droplet_agent = false
  ssh_keys      = [digitalocean_ssh_key.bastion.fingerprint]
  user_data     = <<END
#!/bin/bash

set -euxo pipefail

sed -i -e 's/^#Port .*/Port ${var.ssh_port}/' /etc/ssh/sshd_config
echo -e "PasswordAuthentication no" > /etc/ssh/sshd_config
systemctl restart sshd

for service in "cron unattended-upgrades"; do
  systemctl disable $service
  systemctl stop $service
done
END
}

resource "digitalocean_firewall" "bastion" {
  name = var.firewall_rule_name

  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = var.ssh_port
    source_addresses = ["0.0.0.0/0"]
  }

  tags = concat(var.tags, [])
}

resource "digitalocean_project_resources" "bastion" {
  project = var.do_project_id

  resources = [
    digitalocean_droplet.bastion.urn,
  ]
}
