resource "digitalocean_kubernetes_cluster" "main" {
  name         = var.main_cluster_name
  region       = var.do_region
  version      = var.kubernetes_version
  vpc_uuid     = var.main_private_network_uuid
  tags         = var.do_tags
  auto_upgrade = true
  ha           = true

  node_pool {
    name       = "${var.main_cluster_name}-default"
    size       = var.main_node_size
    node_count = var.main_node_count
    labels     = var.ci_node_count > 0 ? {} : { workload = "ci" }
  }
}

resource "digitalocean_kubernetes_node_pool" "ci" {
  count      = var.ci_node_count > 0 ? 1 : 0
  cluster_id = digitalocean_kubernetes_cluster.main.id

  name       = "${var.main_cluster_name}-ci"
  size       = var.ci_node_size
  node_count = var.ci_node_count
  tags       = concat(var.do_tags, ["ci"])

  labels = {
    workload = "ci"
  }

  taint {
    key    = "workload"
    value  = "ci"
    effect = "NoSchedule"
  }
}

resource "digitalocean_kubernetes_cluster" "ugc" {
  name         = var.ugc_cluster_name
  region       = var.ugc_do_region
  version      = var.kubernetes_version
  vpc_uuid     = var.ugc_private_network_uuid
  tags         = concat(var.do_tags, ["ugc"])
  auto_upgrade = true
  ha           = true

  node_pool {
    name       = "${var.ugc_cluster_name}-default"
    size       = var.ugc_node_size
    node_count = var.ugc_node_count
  }
}

resource "digitalocean_project_resources" "cluster" {
  project = var.do_project_id

  resources = [digitalocean_kubernetes_cluster.main.urn, digitalocean_kubernetes_cluster.ugc.urn]
}

resource "local_sensitive_file" "main_kubeconfig" {
  content         = digitalocean_kubernetes_cluster.main.kube_config.0.raw_config
  filename        = var.main_kubeconfig_filename
  file_permission = "644"
}

resource "local_sensitive_file" "ugc_kubeconfig" {
  content         = digitalocean_kubernetes_cluster.ugc.kube_config.0.raw_config
  filename        = var.ugc_kubeconfig_filename
  file_permission = "644"
}
