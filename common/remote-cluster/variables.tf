variable "do_region" {
  type = string
}

variable "do_tags" {
  type    = list(string)
  default = ["website-tf"]
}

variable "main_private_network_uuid" {
  type = string
}

variable "do_project_id" {
  type = string
}

variable "kubernetes_version" {
  type        = string
  description = "Version of Kubernetes. Use `doctl kubernetes options versions` to see the full list."
}

variable "main_cluster_name" {
  type = string
}

variable "main_node_size" {
  type = string
}

variable "main_node_count" {
  type = number
}

variable "ci_node_size" {
  type = string
}

variable "ci_node_count" {
  type = number
}

variable "main_kubeconfig_filename" {
  type = string
}

variable "ugc_do_region" {
  type = string
}

variable "ugc_private_network_uuid" {
  type = string
}

variable "ugc_cluster_name" {
  type = string
}

variable "ugc_node_size" {
  type = string
}

variable "ugc_node_count" {
  type = number
}

variable "ugc_kubeconfig_filename" {
  type = string
}
