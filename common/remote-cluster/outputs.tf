output "main_kubeconfig" {
  value = abspath(local_sensitive_file.main_kubeconfig.filename)
}

output "ugc_kubeconfig" {
  value = abspath(local_sensitive_file.ugc_kubeconfig.filename)
}
