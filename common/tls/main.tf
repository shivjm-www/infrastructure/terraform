locals {
  allowed_uses = var.is_ca ? [
    "cert_signing",
    "crl_signing",
    ] : [
    "digital_signature",
    "key_encipherment",
    "server_auth"
  ]
}

resource "tls_private_key" "rsa" {
  count     = var.use_ecdsa ? 0 : 1
  algorithm = "RSA"
  rsa_bits  = var.rsa_bits
}

resource "tls_private_key" "ecdsa" {
  count       = var.use_ecdsa ? 1 : 0
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

resource "tls_cert_request" "csr" {
  private_key_pem = var.use_ecdsa ? tls_private_key.ecdsa[0].private_key_pem : tls_private_key.rsa[0].private_key_pem

  subject {
    common_name = var.common_name
  }

  dns_names = var.dns_names
}

resource "tls_locally_signed_cert" "cert" {
  count              = var.ca_private_key == null ? 0 : 1
  cert_request_pem   = tls_cert_request.csr.cert_request_pem
  ca_private_key_pem = var.ca_private_key
  ca_cert_pem        = var.ca_certificate

  validity_period_hours = var.validity_hours

  is_ca_certificate = var.is_ca

  allowed_uses = local.allowed_uses
}

resource "tls_self_signed_cert" "cert" {
  count = var.ca_private_key == null ? 1 : 0

  private_key_pem = var.use_ecdsa ? tls_private_key.ecdsa[0].private_key_pem : tls_private_key.rsa[0].private_key_pem

  validity_period_hours = var.validity_hours

  subject {
    common_name = var.common_name
  }

  dns_names = var.dns_names

  is_ca_certificate = var.is_ca

  allowed_uses = local.allowed_uses
}
