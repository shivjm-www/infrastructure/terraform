output "private_key_pem" {
  value     = trimspace(var.use_ecdsa ? tls_private_key.ecdsa[0].private_key_pem : tls_private_key.rsa[0].private_key_pem)
  sensitive = true
}

output "cert_pem" {
  value     = trimspace(var.ca_private_key == null ? tls_self_signed_cert.cert[0].cert_pem : tls_locally_signed_cert.cert[0].cert_pem)
  sensitive = true
}
