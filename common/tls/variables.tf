variable "use_ecdsa" {
  type    = bool
  default = false
}

variable "rsa_bits" {
  type    = number
  default = 4096
}

variable "common_name" {
  type = string
}

variable "dns_names" {
  type    = list(string)
  default = null
}

variable "validity_hours" {
  type    = number
  default = 24 * 365 * 10
}

variable "is_ca" {
  type    = bool
  default = false
}

variable "ca_certificate" {
  type        = string
  description = "Public key of CA if certificate is not self-signed."
  default     = null
}

variable "ca_private_key" {
  type        = string
  description = "Private key of CA if certificate is not self-signed."
  default     = null
}
