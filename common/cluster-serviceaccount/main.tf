resource "kubernetes_namespace_v1" "sa" {
  metadata {
    name = var.sa_namespace
  }
}

resource "kubernetes_service_account_v1" "sa" {
  metadata {
    name      = var.sa_username
    namespace = var.sa_namespace
  }

  automount_service_account_token = false

  depends_on = [kubernetes_namespace_v1.sa]
}

resource "kubernetes_cluster_role_binding_v1" "sa_admin" {
  metadata {
    name = "${var.sa_username}-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = var.sa_username
    namespace = var.sa_namespace
  }

  depends_on = [kubernetes_service_account_v1.sa]
}

resource "kubernetes_secret_v1" "sa" {
  metadata {
    name      = "${var.sa_username}-serviceaccount-token"
    namespace = var.sa_namespace

    annotations = {
      "kubernetes.io/service-account.name" = var.sa_username
    }
  }

  type = "kubernetes.io/service-account-token"

  depends_on = [kubernetes_service_account_v1.sa]
}

data "kubernetes_secret_v1" "sa_serviceaccount" {
  metadata {
    name      = "${var.sa_username}-serviceaccount-token"
    namespace = var.sa_namespace
  }

  depends_on = [kubernetes_secret_v1.sa]
}
