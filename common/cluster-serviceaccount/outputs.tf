output "service_account_token" {
  sensitive = true
  value     = lookup(data.kubernetes_secret_v1.sa_serviceaccount.data, "token")
}

output "ca_certificate" {
  sensitive = true
  value     = lookup(data.kubernetes_secret_v1.sa_serviceaccount.data, "ca.crt")
}
