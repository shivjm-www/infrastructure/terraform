variable "do_region" {
  type = string
}

variable "private_network_uuid" {
  type      = string
  sensitive = true
}

variable "cluster_name" {
  type = string
}

variable "cluster_engine_version" {
  type = string
}

variable "cluster_node_size" {
  type = string
}

variable "cluster_node_count" {
  type = number
}

variable "k8s_cluster_id" {
  type        = string
  description = "ID of Kubernetes cluster to allow requests from."
}
