resource "digitalocean_database_cluster" "postgres" {
  name                 = var.cluster_name
  engine               = "pg"
  version              = var.cluster_engine_version
  size                 = var.cluster_node_size
  region               = var.do_region
  node_count           = var.cluster_node_count
  private_network_uuid = var.private_network_uuid
}

resource "digitalocean_database_firewall" "postgres_firewall" {
  cluster_id = digitalocean_database_cluster.postgres.id

  rule {
    type  = "k8s"
    value = var.k8s_cluster_id
  }
}
