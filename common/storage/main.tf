resource "digitalocean_spaces_bucket" "loki" {
  name   = var.loki_bucket_name
  region = var.do_region

  lifecycle_rule {
    enabled = true
    expiration {
      days = var.loki_bucket_expiration
    }
  }

  acl = "private"
}

resource "digitalocean_spaces_bucket" "mimir" {
  name   = var.mimir_bucket_name
  region = var.do_region

  acl = "private"
}

resource "digitalocean_spaces_bucket" "tempo" {
  name   = var.tempo_bucket_name
  region = var.do_region

  acl = "private"
}

resource "digitalocean_project_resources" "buckets" {
  project = var.do_project_id

  resources = [
    digitalocean_spaces_bucket.mimir.urn,
    digitalocean_spaces_bucket.tempo.urn,
    digitalocean_spaces_bucket.loki.urn
  ]
}
