variable "database" {
  type        = string
  description = "Name of database to create."
}

variable "username" {
  type        = string
  description = "Name of admin user to create."
}

variable "password" {
  type        = string
  description = "Password to set for admin user."
}

variable "buckets" {
  type = list(string)
}
