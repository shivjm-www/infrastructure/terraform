output "timescaledb_port" {
  value = docker_container.timescaledb.ports[0].external
}

output "s3mock_port" {
  value = docker_container.s3mock.ports[0].external
}
