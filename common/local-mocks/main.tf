resource "docker_container" "timescaledb" {
  name  = "timescaledb"
  image = docker_image.timescaledb.repo_digest

  command = [
    # "-c",
    # "log_statement=all"
  ]

  env = [
    "POSTGRES_DB=${var.database}",
    "POSTGRES_USER=${var.username}",
    "POSTGRES_PASSWORD=${var.password}",
  ]

  ports {
    internal = 5432
  }

  volumes {
    container_path = "/docker-entrypoint-initdb.d/002_install_promscale.sql"
    host_path      = abspath("${path.module}/initialize-promscale.sql")
    read_only      = true
  }
}

resource "docker_container" "s3mock" {
  name  = "s3mock"
  image = docker_image.s3mock.repo_digest

  env = [
    "INITIAL_BUCKETS=${join(",", var.buckets)}"
  ]

  ports {
    internal = 9090
  }
}

resource "docker_image" "timescaledb" {
  name          = data.docker_registry_image.timescaledb.name
  keep_locally  = true
  pull_triggers = [data.docker_registry_image.timescaledb.sha256_digest]
}

resource "docker_image" "s3mock" {
  name          = data.docker_registry_image.s3mock.name
  keep_locally  = true
  pull_triggers = [data.docker_registry_image.s3mock.sha256_digest]
}

data "docker_registry_image" "timescaledb" {
  name = "timescale/timescaledb-ha:pg14.5-ts2.8.0-latest"
}

data "docker_registry_image" "s3mock" {
  name = "adobe/s3mock:2.6.1"
}
