resource "b2_bucket" "discourse_uploads" {
  bucket_name = var.discourse_uploads_bucket_name
  bucket_type = "allPublic"

  lifecycle_rules {
    file_name_prefix             = ""
    days_from_hiding_to_deleting = var.discourse_uploads_delete_hidden_files_after
  }
}
