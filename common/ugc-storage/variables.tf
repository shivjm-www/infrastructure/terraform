variable "discourse_uploads_bucket_name" {
  type = string
}

variable "discourse_uploads_delete_hidden_files_after" {
  type        = number
  description = "Number of days between a `DELETE` operation and when the file goes from hidden to deleted."
}
