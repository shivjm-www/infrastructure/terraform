variable "do_region" {
  type = string
}

variable "name" {
  type        = string
  default     = "website-tf"
  description = "The name to give the VPC."
}

variable "main_vpc_ip_range" {
  type        = string
  description = "The IP range to use within the VPC."
}
