resource "digitalocean_vpc" "website" {
  name     = var.name
  region   = var.do_region
  ip_range = var.main_vpc_ip_range
}
