output "urn" {
  value = digitalocean_vpc.website.urn
}

output "id" {
  value = digitalocean_vpc.website.id
}
