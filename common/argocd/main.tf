locals {
  namespaces_to_create     = ["linkerd", "external-dns", "observability", "gitlab-runner", "contour", "github-runner", "feeds", "rustypaste"]
  ugc_namespaces_to_create = ["linkerd", "external-dns", "observability", "contour", "discourse"]
  revision                 = var.services_deployment_revision
  letsencryptServers = {
    "staging"    = "https://acme-staging-v02.api.letsencrypt.org/directory",
    "production" = "https://acme-v02.api.letsencrypt.org/directory"
  }
  ugc_cluster_ca_certificate_raw           = trim(trim(var.ugc_cluster_ca_certificate, "----BEGIN CERTIFICATE---"), "----END CERTIFICATE----")
  ugc_cluster_ca_certificate_reconstructed = "-----BEGIN CERTIFICATE-----\n${replace(local.ugc_cluster_ca_certificate_raw, " ", "\n")}\n-----END CERTIFICATE-----"
  ugc_cluster_config = {
    bearerToken = var.ugc_cluster_bearer_token
    tlsClientConfig = {
      caData = base64encode(var.ugc_cluster_ca_certificate)
    }
  }
}

data "kustomization_overlay" "argocd" {
  config_map_generator {
    name      = "argocd-cmd-params-cm"
    namespace = "argocd"
    behavior  = "merge"

    literals = ["server.enable.gzip=${var.enable_gzip}"]
  }

  secret_generator {
    name      = "argocd-secret"
    namespace = "argocd"
    behavior  = "merge"
    literals = [
      "webhook.gitlab.secret=${var.argocd_webhook_secret}",
      "webhook.github.secret=${var.argocd_webhook_secret}",
    ]

    options {
      disable_name_suffix_hash = true
    }
  }

  secret_generator {
    name      = "argocd-notifications-secret"
    namespace = "argocd"
    behavior  = "merge"
    literals  = ["discord-webhook-path=${var.argocd_discord_webhook_url}"]

    options {
      disable_name_suffix_hash = true
    }
  }

  secret_generator {
    name      = "gitlab-shivjm-www-services"
    namespace = "argocd"
    options {
      labels = {
        "argocd.argoproj.io/secret-type" = "repository"
      }
      disable_name_suffix_hash = true
    }

    literals = [
      "url=https://gitlab.com/shivjm-www/infrastructure/services.git",
      "username=${var.gitlab_username}",
      "password=${var.gitlab_password}",
      "type=git"
    ]
  }

  secret_generator {
    name      = "gitlab-shivjm-www-observability"
    namespace = "argocd"
    options {
      labels = {
        "argocd.argoproj.io/secret-type" = "repository"
      }
      disable_name_suffix_hash = true
    }

    literals = [
      "url=https://gitlab.com/shivjm-www/infrastructure/observability.git",
      "username=${var.gitlab_username}",
      "password=${var.gitlab_password}",
      "type=git"
    ]
  }

  secret_generator {
    name      = "ghcr-io-oci-repository"
    namespace = "argocd"
    options {
      labels = {
        "argocd.argoproj.io/secret-type" = "repository"
      }
      disable_name_suffix_hash = true
    }

    literals = [
      "url=ghcr.io",
      # "username=${var.gitlab_username}",
      # "password=${var.gitlab_password}",
      "type=helm",
      "enableOCI=true",
    ]
  }

  secret_generator {
    name      = "main-cluster-config"
    namespace = "argocd"

    options {
      labels = {
        "argocd.argoproj.io/secret-type" = "cluster"
        "cluster-type"                   = "main"
      }
      disable_name_suffix_hash = true
    }

    literals = [
      "name=main",
      "server=https://kubernetes.default.svc",
      "config=${var.main_cluster_config}"
    ]
  }

  secret_generator {
    name      = "ugc-cluster-config"
    namespace = "argocd"
    options {
      labels = {
        "argocd.argoproj.io/secret-type" = "cluster"
        "cluster-type"                   = "ugc"
      }
      disable_name_suffix_hash = true
    }

    literals = [
      "name=ugc",
      "server=${var.ugc_cluster_api_server}",
      "config=${jsonencode(local.ugc_cluster_config)}"
    ]
  }

  resources = [
    "${path.module}/manifests/argocd",
  ]
}

resource "kubernetes_namespace" "secret_namespaces" {
  for_each = toset(local.namespaces_to_create)

  metadata {
    name = each.value
  }

  lifecycle {
    ignore_changes = [metadata]
  }
}

resource "kubernetes_namespace" "secret_namespaces_ugc" {
  provider = kubernetes.ugc

  for_each = toset(local.ugc_namespaces_to_create)

  metadata {
    name = each.value
  }

  lifecycle {
    ignore_changes = [metadata]
  }
}

resource "kustomization_resource" "argocd0" {
  for_each = data.kustomization_overlay.argocd.ids_prio[0]

  manifest = data.kustomization_overlay.argocd.manifests[each.value]

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kustomization_resource" "argocd1" {
  for_each = data.kustomization_overlay.argocd.ids_prio[1]

  manifest = data.kustomization_overlay.argocd.manifests[each.value]

  depends_on = [kustomization_resource.argocd0]
}

resource "kustomization_resource" "argocd2" {
  for_each = data.kustomization_overlay.argocd.ids_prio[2]

  manifest = data.kustomization_overlay.argocd.manifests[each.value]

  depends_on = [kustomization_resource.argocd1]
}
