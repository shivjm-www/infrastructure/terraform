variable "enable_gzip" {
  type = bool
}

variable "do_token" {
  type      = string
  sensitive = true
}

variable "do_region" {
  type = string
}

variable "do_spaces_access_id" {
  type      = string
  sensitive = true
}

variable "do_spaces_secret_key" {
  type      = string
  sensitive = true
}

variable "allow_local_s3" {
  type = bool
}

variable "s3_endpoint" {
  type    = string
  default = null
}

variable "kubeconfig_path" {
  type = string
}

variable "gitlab_username" {
  type      = string
  sensitive = true
}

variable "gitlab_password" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_token" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_do_token" {
  type      = string
  sensitive = true
}

variable "services_deployment_revision" {
  type = string
}

variable "argocd_keyfile" {
  type      = string
  sensitive = true
}

variable "main_domain" {
  type    = string
  default = null
}

variable "dev_domain" {
  type    = string
  default = null
}

variable "loki_bucket_name" {
  type      = string
  sensitive = true
}

variable "mimir_bucket_name" {
  type      = string
  sensitive = true
}

variable "tempo_bucket_name" {
  type      = string
  sensitive = true
}

variable "loki_replicas" {
  type = number
}

variable "prometheus_replicas" {
  type    = number
  default = 1
}

variable "enable_additional_scrape_configs" {
  type = bool
}

variable "argocd_webhook_secret" {
  type      = string
  sensitive = true
}

variable "argocd_discord_webhook_url" {
  type      = string
  sensitive = true
}

variable "cluster_digitalocean_token" {
  type      = string
  sensitive = true
}

variable "github_runner_token" {
  type      = string
  sensitive = true
}

variable "github_runner_webhook_token" {
  type      = string
  sensitive = true
}

variable "github_workflow_access_tokens" {
  type      = map(string)
  sensitive = true
}

variable "ghcr_io_registry_config" {
  type      = string
  sensitive = true
}

variable "gitlab_ci_pipelines_exporter_token" {
  type      = string
  sensitive = true
}

variable "gitlab_ci_pipelines_exporter_webhook_token" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_s3_bucket" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_s3_access_key" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_s3_secret_key" {
  type      = string
  sensitive = true
}

variable "grafana_admin_username" {
  type      = string
  sensitive = true
}

variable "grafana_admin_password" {
  type      = string
  sensitive = true
}

variable "feeds_s3_access_key_id" {
  type      = string
  sensitive = true
}

variable "feeds_s3_secret_access_key" {
  type      = string
  sensitive = true
}

variable "feeds_s3_bucket" {
  type      = string
  sensitive = true
}

variable "feeds_s3_endpoint" {
  type      = string
  sensitive = true
  default   = null
}

variable "use_production_tls" {
  type    = bool
  default = false
}

variable "enable_linkerd_cni" {
  type    = bool
  default = false
}

variable "linkerd_replicas" {
  type = number
}

variable "enable_observability_stack" {
  type = bool
}

variable "observability_repository_revision" {
  type = string
}

variable "main_cluster_config" {
  type      = string
  sensitive = true
}

variable "ugc_cluster_digitalocean_token" {
  type      = string
  sensitive = true
}

variable "ugc_cluster_api_server" {
  type      = string
  sensitive = true
}

variable "ugc_cluster_bearer_token" {
  type      = string
  sensitive = true
}

variable "ugc_cluster_ca_certificate" {
  type      = string
  sensitive = true
}

variable "ugc_domain" {
  type = string
}

variable "ugc_discourse_admin_username" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_admin_email" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_admin_password" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_pg_host" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_pg_port" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_pg_username" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_pg_database" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_pg_password" {
  type      = string
  sensitive = true
}

variable "ugc_smtp_host" {
  type      = string
  sensitive = true
}

variable "ugc_smtp_port" {
  type      = string
  sensitive = true
}

variable "ugc_smtp_username" {
  type      = string
  sensitive = true
}

variable "ugc_smtp_password" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_install" {
  type = bool
}

variable "ugc_discourse_s3_region" {
  type = string
}

variable "ugc_discourse_s3_endpoint" {
  type = string
}

variable "ugc_discourse_s3_access_key_id" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_s3_secret_access_key" {
  type      = string
  sensitive = true
}

variable "ugc_discourse_s3_cdn_url" {
  type = string
}

variable "ugc_discourse_s3_upload_bucket" {
  type = string
}

variable "ugc_discourse_s3_backup_bucket" {
  type      = string
  sensitive = true
}

variable "ugc_cloudflare_dns_api_token" {
  type      = string
  sensitive = true
}

variable "ugc_linkerd_replicas" {
  type = number
}

variable "rustypaste_token" {
  type      = string
  sensitive = true
}
