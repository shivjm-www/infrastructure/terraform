locals {
  additionalScrapeConfigs = []
  s3_endpoint             = var.s3_endpoint == null ? "https://${var.do_region}.digitaloceanspaces.com" : var.s3_endpoint
  s3_endpoint_no_protocol = replace(local.s3_endpoint, "/^https?:\\/\\//", "")
}

module "linkerd_trust_root_tls" {
  source = "../tls"

  use_ecdsa   = true
  is_ca       = true
  common_name = "root.linkerd.cluster.local"
}

resource "kubernetes_secret" "linkerd_trust_root" {
  metadata {
    name      = "linkerd-trust-anchor"
    namespace = "linkerd"
  }

  data = {
    "tls.crt" = module.linkerd_trust_root_tls.cert_pem,
    "tls.key" = module.linkerd_trust_root_tls.private_key_pem,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_config_map" "linkerd_identity_trust_roots" {
  metadata {
    namespace = "linkerd"
    name      = "linkerd-identity-trust-roots"
  }

  data = {
    "ca-bundle.crt" = module.linkerd_trust_root_tls.cert_pem
  }
}

module "contour_ca_tls" {
  source = "../tls"

  common_name = "contour.cluster.local"
  is_ca       = true
}

resource "kubernetes_secret" "contour_ca" {
  metadata {
    name      = "contour-ca"
    namespace = "contour"
  }

  data = {
    "tls.crt" = module.contour_ca_tls.cert_pem,
    "tls.key" = module.contour_ca_tls.private_key_pem,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "loki_s3" {
  metadata {
    name      = "loki-s3"
    namespace = "observability"
  }

  data = {
    "s3_endpoint"          = local.s3_endpoint,
    "s3_bucket"            = var.loki_bucket_name,
    "s3_access_key_id"     = var.do_spaces_access_id,
    "s3_secret_access_key" = var.do_spaces_secret_key,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "mimir_s3" {
  metadata {
    name      = "mimir-s3"
    namespace = "observability"
  }

  data = {
    "s3_endpoint"          = local.s3_endpoint_no_protocol,
    "s3_bucket"            = var.mimir_bucket_name,
    "s3_access_key_id"     = var.do_spaces_access_id,
    "s3_secret_access_key" = var.do_spaces_secret_key,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "tempo_s3" {
  metadata {
    name      = "tempo-s3"
    namespace = "observability"
  }

  data = {
    "s3_endpoint"          = local.s3_endpoint_no_protocol,
    "s3_bucket"            = var.tempo_bucket_name,
    "s3_access_key_id"     = var.do_spaces_access_id,
    "s3_secret_access_key" = var.do_spaces_secret_key,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "gitlab_runner" {
  metadata {
    name      = "gitlab-runner-group-token"
    namespace = "gitlab-runner"
  }

  data = {
    "runner-registration-token" = "",
    "runner-token"              = var.gitlab_runner_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "gitlab_runner_do_token" {
  metadata {
    name      = "gitlab-runner-do-token"
    namespace = "gitlab-runner"
  }

  data = {
    "do_token" = var.gitlab_runner_do_token
  }

  depends_on = [
    kubernetes_namespace.secret_namespaces
  ]
}

resource "kubernetes_secret" "contour_cert_manager_digitalocean" {
  metadata {
    name      = "digitalocean"
    namespace = "contour"
  }

  data = {
    "access-token" = var.cluster_digitalocean_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "external_dns_digitalocean" {
  metadata {
    name      = "digitalocean"
    namespace = "external-dns"
  }

  data = {
    "digitalocean_api_token" = var.cluster_digitalocean_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "github_runner_tokens" {
  metadata {
    name      = "auth"
    namespace = "github-runner"
  }

  data = {
    "github_token"                = var.github_runner_token
    "github_webhook_secret_token" = var.github_runner_webhook_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "github_runner_workflow_dispatch_auth" {
  metadata {
    name      = "workflow-dispatch-auth"
    namespace = "github-runner"
  }

  data = var.github_workflow_access_tokens

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "feeds_github_registry" {
  metadata {
    name      = "github-shivjm"
    namespace = "feeds"
  }

  data = {
    ".dockerconfigjson" = var.ghcr_io_registry_config
  }

  type = "kubernetes.io/dockerconfigjson"

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "gitlab_ci_pipelines_exporter" {
  metadata {
    name      = "gitlab-ci-pipelines-exporter"
    namespace = "observability"
  }

  data = {
    "gitlabToken"  = var.gitlab_ci_pipelines_exporter_token,
    "webhookToken" = var.gitlab_ci_pipelines_exporter_webhook_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "gitlab_runner_s3" {
  metadata {
    name      = "s3access"
    namespace = "gitlab-runner"
  }

  data = {
    "accesskey" = var.gitlab_runner_s3_access_key,
    "secretkey" = var.gitlab_runner_s3_secret_key,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "grafana_admin_credentials" {
  metadata {
    name      = "grafana-admin-credentials"
    namespace = "observability"
  }

  data = {
    user     = var.grafana_admin_username,
    password = var.grafana_admin_password
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}

resource "kubernetes_secret" "feeds_s3" {
  metadata {
    name      = "feeds-s3"
    namespace = "feeds"
  }

  data = {
    "AWS_ACCESS_KEY_ID"     = var.feeds_s3_access_key_id,
    "AWS_SECRET_ACCESS_KEY" = var.feeds_s3_secret_access_key,
    "AWS_REGION"            = var.do_region,
    "S3_BUCKET"             = var.feeds_s3_bucket,
    "S3_ENDPOINT"           = local.s3_endpoint,
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}


module "linkerd_trust_root_tls_ugc" {
  source = "../tls"

  use_ecdsa   = true
  is_ca       = true
  common_name = "root.linkerd.cluster.local"
}

resource "kubernetes_secret" "linkerd_trust_root_ugc" {
  provider = kubernetes.ugc

  metadata {
    name      = "linkerd-trust-anchor"
    namespace = "linkerd"
  }

  data = {
    "tls.crt" = module.linkerd_trust_root_tls_ugc.cert_pem,
    "tls.key" = module.linkerd_trust_root_tls_ugc.private_key_pem,
  }

  depends_on = [kubernetes_namespace.secret_namespaces_ugc]
}

resource "kubernetes_config_map" "linkerd_identity_trust_roots_ugc" {
  provider = kubernetes.ugc

  metadata {
    namespace = "linkerd"
    name      = "linkerd-identity-trust-roots"
  }

  data = {
    "ca-bundle.crt" = module.linkerd_trust_root_tls_ugc.cert_pem
  }
}

module "contour_ca_tls_ugc" {
  source = "../tls"

  common_name = "contour.cluster.local"
  is_ca       = true
}

resource "kubernetes_secret" "contour_ca_ugc" {
  provider = kubernetes.ugc

  metadata {
    name      = "contour-ca"
    namespace = "contour"
  }

  data = {
    "tls.crt" = module.contour_ca_tls_ugc.cert_pem,
    "tls.key" = module.contour_ca_tls_ugc.private_key_pem,
  }

  depends_on = [kubernetes_namespace.secret_namespaces_ugc]
}

resource "kubernetes_secret" "ugc_discourse_password" {
  provider = kubernetes.ugc

  metadata {
    name      = "credentials"
    namespace = "discourse"
  }

  data = {
    "discourse-password" = var.ugc_discourse_admin_password
    "smtp-password"      = var.ugc_smtp_password
  }

  depends_on = [kubernetes_namespace.secret_namespaces_ugc]
}

resource "kubernetes_secret" "ugc_discourse_pg" {
  provider = kubernetes.ugc

  metadata {
    name      = "postgresql"
    namespace = "discourse"
  }

  data = {
    "password"          = var.ugc_discourse_pg_password
    "postgres-password" = var.ugc_discourse_pg_password
  }
}

resource "kubernetes_secret" "ugc_contour_cert_manager_cloudflare" {
  provider = kubernetes.ugc

  metadata {
    name      = "cloudflare"
    namespace = "contour"
  }

  data = {
    "api-token" = var.ugc_cloudflare_dns_api_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces_ugc]
}

resource "kubernetes_secret" "ugc_external_dns_cloudflare" {
  provider = kubernetes.ugc

  metadata {
    name      = "cloudflare"
    namespace = "external-dns"
  }

  data = {
    "cloudflare_api_token" = var.ugc_cloudflare_dns_api_token
  }

  depends_on = [kubernetes_namespace.secret_namespaces_ugc]
}

resource "kubernetes_secret" "ugc_discourse_s3" {
  provider = kubernetes.ugc

  metadata {
    name      = "discourse-s3"
    namespace = "discourse"
  }

  data = {
    "DISCOURSE_S3_INSTALL_CORS_RULE"          = "false",
    "DISCOURSE_S3_REGION"                     = var.ugc_discourse_s3_region, # TODO: Remove this if not required.
    "DISCOURSE_S3_CONFIGURE_TOMBSTONE_POLICY" = "false",
    "DISCOURSE_S3_ENDPOINT"                   = var.ugc_discourse_s3_endpoint,
    "DISCOURSE_S3_ACCESS_KEY_ID"              = var.ugc_discourse_s3_access_key_id,
    "DISCOURSE_S3_SECRET_ACCESS_KEY"          = var.ugc_discourse_s3_secret_access_key,
    "DISCOURSE_S3_CDN_URL"                    = var.ugc_discourse_s3_cdn_url,
    "DISCOURSE_S3_BUCKET"                     = var.ugc_discourse_s3_upload_bucket,
    "DISCOURSE_S3_UPLOAD_BUCKET"              = var.ugc_discourse_s3_upload_bucket,
    "DISCOURSE_S3_BACKUP_BUCKET"              = var.ugc_discourse_s3_backup_bucket,
  }
}

resource "kubernetes_secret" "rustypaste_config" {
  metadata {
    name      = "rustypaste"
    namespace = "rustypaste"
  }

  data = {
    "config.toml" = templatefile("${path.module}/rustypaste-template.toml", {
      "domain"     = var.main_domain,
      "auth_token" = var.rustypaste_token,
    })
  }

  depends_on = [kubernetes_namespace.secret_namespaces]
}
