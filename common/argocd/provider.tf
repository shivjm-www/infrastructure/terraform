terraform {
  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = "0.9.0"
    }

    # TODO remove kubectl provider when Kustomize `replacements` can
    # be used: https://github.com/kubernetes-sigs/kustomize/issues/4099
    kubectl = {
      source  = "alekc/kubectl"
      version = ">= 2.0.4"
    }

    kubernetes = {
      source                = "kubernetes"
      version               = "2.29.0"
      configuration_aliases = [kubernetes, kubernetes.ugc]
    }

    tls = {
      source  = "hashicorp/tls"
      version = "4.0.3"
    }
  }
}
