locals {
  project = {
    apiVersion = "argoproj.io/v1alpha1",
    kind       = "AppProject"
    metadata = {
      name      = "default",
      namespace = "argocd"
    }
    spec = {
      sourceRepos = ["*"],
      destinations = [
        { namespace = "*", server = "*" }
      ],
      clusterResourceWhitelist = [{ group = "*", kind = "*" }],
    },
  }
  tlas = [
    ["contourEnabled", var.main_domain != null],
    ["domain", var.main_domain == null ? "" : var.main_domain],
    ["devDomain", var.dev_domain == null ? "" : var.dev_domain],
    ["letsencryptServer", var.use_production_tls ? local.letsencryptServers["production"] : local.letsencryptServers["staging"]],
    ["linkerdCni", var.enable_linkerd_cni],
    ["linkerdReplicas", var.linkerd_replicas],
    ["prometheusReplicas", var.prometheus_replicas],
    ["observabilityRevision", var.observability_repository_revision],
    ["lokiReplicas", var.loki_replicas],
    ["gitlabRunnerS3Endpoint", local.s3_endpoint],
    ["gitlabRunnerS3Bucket", var.gitlab_runner_s3_bucket],
    ["localS3", var.allow_local_s3],
    ["enableObservabilityStack", var.enable_observability_stack]
  ]
}

# TODO use `kubernetes_manifest` once
# https://github.com/hashicorp/terraform-provider-kubernetes/issues/1367
# is resolved

# Manually create the default project.
# https://github.com/argoproj/argo-cd/discussions/8402
resource "kubectl_manifest" "default_project" {
  yaml_body = yamlencode(local.project)

  depends_on = [kustomization_resource.argocd2]
}

module "main_application" {
  source = "../argocd-application"

  application_name            = "shivjm-apps"
  target_cluster_type         = "main"
  application_repository      = "https://gitlab.com/shivjm-www/infrastructure/services.git"
  application_repository_path = "apps/main"
  application_revision        = local.revision
  name_prefix                 = ""
  tlas                        = local.tlas

  depends_on = [
    kubectl_manifest.default_project
  ]
}

module "ugc_application" {
  source = "../argocd-application"

  application_name            = "shivjm-ugc"
  target_cluster_type         = "main"
  application_repository      = "https://gitlab.com/shivjm-www/infrastructure/services.git"
  application_repository_path = "apps/ugc"
  application_revision        = local.revision
  destination_cluster         = "ugc"
  name_prefix                 = "ugc-"
  tlas = [
    ["domain", var.ugc_domain],
    ["letsencryptServer", var.use_production_tls ? local.letsencryptServers["production"] : local.letsencryptServers["staging"]],
    ["linkerdReplicas", var.ugc_linkerd_replicas],
    ["prometheusReplicas", 1],
    ["lokiReplicas", 1],
    ["observabilityRevision", var.observability_repository_revision],
    ["revision", local.revision],
    ["installDiscourse", var.ugc_discourse_install],
    ["discoursePgConfiguration", {
      "host"     = var.ugc_discourse_pg_host,
      "port"     = var.ugc_discourse_pg_port,
      "username" = var.ugc_discourse_pg_username,
      "database" = var.ugc_discourse_pg_database,
    }],
    ["smtpConfiguration", {
      "host"     = var.ugc_smtp_host,
      "port"     = var.ugc_smtp_port,
      "username" = var.ugc_smtp_username,
    }],
    ["discourseAdminUser", {
      "username" = var.ugc_discourse_admin_username,
      "email"    = var.ugc_discourse_admin_email,
    }],
    ["enableObservabilityStack", var.enable_observability_stack]
  ]

  depends_on = [
    kubectl_manifest.default_project
  ]
}
