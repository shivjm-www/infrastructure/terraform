variable "enable_gzip" {
  type    = bool
  default = true
}

variable "do_token" {
  type      = string
  sensitive = true
}

variable "do_region" {
  type = string
}

variable "do_spaces_access_id" {
  type      = string
  sensitive = true
}

variable "do_spaces_secret_key" {
  type      = string
  sensitive = true
}

variable "s3_host" {
  type = string
}

variable "kubeconfig_path" {
  type    = string
  default = "../kubeconfig"
}

variable "gitlab_username" {
  type = string
}

variable "gitlab_password" {
  type = string
}

variable "services_deployment_revision" {
  type = string
}

variable "argocd_keyfile" {
  type      = string
  sensitive = true
}

variable "overlay" {
  type    = string
  default = null
}

variable "cluster_digitalocean_token" {
  type      = string
  sensitive = true
}

variable "github_runner_token" {
  type      = string
  sensitive = true
}

variable "github_runner_webhook_token" {
  type      = string
  sensitive = true
}

variable "ghcr_io_registry_config" {
  type      = string
  sensitive = true
}

variable "gitlab_ci_pipelines_exporter_token" {
  type      = string
  sensitive = true
}

variable "gitlab_ci_pipelines_exporter_webhook_token" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_token" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_s3_access_key" {
  type      = string
  sensitive = true
}

variable "gitlab_runner_s3_secret_key" {
  type      = string
  sensitive = true
}

variable "grafana_admin_username" {
  type      = string
  sensitive = true
}

variable "grafana_admin_password" {
  type      = string
  sensitive = true
}

variable "feeds_s3_access_key_id" {
  type      = string
  sensitive = true
}

variable "feeds_s3_secret_access_key" {
  type      = string
  sensitive = true
}

variable "feeds_s3_bucket" {
  type      = string
  sensitive = true
}

variable "feeds_s3_endpoint" {
  type      = string
  sensitive = true
  default   = null
}

variable "observability_repository_revision" {
  type    = string
  default = "main"
}
