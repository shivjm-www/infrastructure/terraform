data "terraform_remote_state" "mocks" {
  backend = "local"

  config = {
    path = "../mocks/terraform.tfstate"
  }
}

module "argocd" {
  source = "../../../common/argocd"

  enable_gzip                                = var.enable_gzip
  do_token                                   = var.do_token
  do_region                                  = var.do_region
  do_spaces_access_id                        = var.do_spaces_access_id
  do_spaces_secret_key                       = var.do_spaces_secret_key
  s3_endpoint                                = "${var.s3_host}:${data.terraform_remote_state.mocks.outputs.s3mock_port}"
  kubeconfig_path                            = var.kubeconfig_path
  gitlab_username                            = var.gitlab_username
  gitlab_password                            = var.gitlab_password
  gitlab_runner_token                       = var.gitlab_runner_token
  services_deployment_revision               = var.services_deployment_revision
  argocd_keyfile                             = var.argocd_keyfile
  loki_bucket_name                           = "loki"
  loki_replicas                              = 1
  cluster_ca_certificate_bundle              = ""
  argocd_webhook_secret                      = ""
  argocd_discord_webhook_url                 = ""
  github_runner_token                        = var.github_runner_token
  github_runner_webhook_token                = var.github_runner_webhook_token
  feeds_s3_access_key_id                     = var.feeds_s3_access_key_id
  feeds_s3_secret_access_key                 = var.feeds_s3_secret_access_key
  gitlab_runner_s3_bucket                    = "gitlabrunner"
  gitlab_runner_s3_access_key                = var.gitlab_runner_s3_access_key
  gitlab_runner_s3_secret_key                = var.gitlab_runner_s3_secret_key
  gitlab_ci_pipelines_exporter_token         = var.gitlab_ci_pipelines_exporter_token
  gitlab_ci_pipelines_exporter_webhook_token = var.gitlab_ci_pipelines_exporter_webhook_token
  grafana_admin_username                     = var.grafana_admin_username
  grafana_admin_password                     = var.grafana_admin_password
  cluster_digitalocean_token                 = var.cluster_digitalocean_token
  feeds_s3_bucket                            = var.feeds_s3_bucket
  ghcr_io_registry_config                    = var.ghcr_io_registry_config
  feeds_s3_endpoint                          = "${var.s3_host}:${data.terraform_remote_state.mocks.outputs.s3mock_port}"
  observability_repository_revision          = var.observability_repository_revision

  providers = {
    kubernetes    = kubernetes
    kubectl       = kubectl
    kustomization = kustomization
  }
}
