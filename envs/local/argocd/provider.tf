terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.17.0"
    }

    kustomization = {
      source  = "kbst/kustomization"
      version = "0.9.0"
    }

    # TODO remove kubectl provider when Kustomize `replacements` can
    # be used: https://github.com/kubernetes-sigs/kustomize/issues/4099
    kubectl = {
      source  = "alekc/kubectl"
      version = ">= 2.0.4"
    }

    kubernetes = {
      source  = "kubernetes"
      version = "2.29.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "kustomization" {
  kubeconfig_path = var.kubeconfig_path
}

provider "kubectl" {
  config_path = var.kubeconfig_path
}

provider "kubernetes" {
  config_path = var.kubeconfig_path
}
