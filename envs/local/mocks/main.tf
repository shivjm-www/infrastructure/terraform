module "local-mocks" {
  source = "../../../common/local-mocks"

  database = var.database
  username = var.username
  password = var.password
  buckets  = var.buckets
}
