variable "docker_host" {
  type = string
}

variable "database" {
  type        = string
  description = "Name of database to create."
  default     = "prometheus"
}

variable "username" {
  type        = string
  description = "Name of admin user to create."
  default     = "prometheus"
}

variable "password" {
  type        = string
  description = "Password to set for admin user."
  default     = "1234"
}

variable "buckets" {
  type = list(string)
  default = [
    "loki",
    "feeds",
    "gitlabrunner"
  ]
}
