terraform {
  backend "http" {
  }

  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = "0.9.0"
    }

    # TODO remove kubectl provider when Kustomize `replacements` can
    # be used: https://github.com/kubernetes-sigs/kustomize/issues/4099
    kubectl = {
      source  = "alekc/kubectl"
      version = ">= 2.0.4"
    }

    kubernetes = {
      source                = "kubernetes"
      version               = "2.29.0"
      configuration_aliases = [kubernetes, kubernetes.ugc]
    }
  }
}

provider "kustomization" {
  kubeconfig_path = var.kubeconfig_path
}

provider "kubectl" {
  config_path = var.kubeconfig_path
}

provider "kubectl" {
  alias       = "ugc"
  config_path = var.ugc_kubeconfig_path
}

provider "kubernetes" {
  config_path = var.kubeconfig_path
}

provider "kubernetes" {
  alias = "ugc"

  config_path = var.ugc_kubeconfig_path
}
