output "private_host" {
  value     = module.database.private_host
  sensitive = true
}

output "port" {
  value     = module.database.port
  sensitive = true
}

output "user" {
  value     = module.database.user
  sensitive = true
}

output "password" {
  value     = module.database.password
  sensitive = true
}

output "database" {
  value     = module.database.database
  sensitive = true
}
