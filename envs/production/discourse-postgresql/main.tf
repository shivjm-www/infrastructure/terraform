module "database" {
  source = "../../../common/remote-postgresql"

  do_region              = var.do_ugc_region
  private_network_uuid   = var.private_network_uuid
  cluster_name           = var.cluster_name
  cluster_engine_version = var.cluster_engine_version
  cluster_node_size      = var.cluster_node_size
  cluster_node_count     = var.cluster_node_count
  k8s_cluster_id         = var.k8s_cluster_id
}
