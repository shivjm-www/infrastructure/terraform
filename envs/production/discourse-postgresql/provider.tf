terraform {
  backend "http" {}

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.30.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}
