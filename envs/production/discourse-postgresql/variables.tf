variable "do_ugc_region" {
  type = string
}

variable "do_token" {
  type      = string
  sensitive = true
}

variable "private_network_uuid" {
  type      = string
  sensitive = true
}

variable "cluster_name" {
  type    = string
  default = "ugc-discourse"
}

variable "cluster_engine_version" {
  type    = string
  default = "15"
}

variable "cluster_node_size" {
  type = string
}

variable "cluster_node_count" {
  type    = number
  default = 1
}

variable "k8s_cluster_id" {
  type        = string
  description = "ID of Kubernetes cluster to allow requests from."
}
