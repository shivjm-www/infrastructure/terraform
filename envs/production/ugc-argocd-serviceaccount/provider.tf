terraform {
  backend "http" {
  }

  required_providers {
    kubernetes = {
      source  = "kubernetes"
      version = "2.29.0"
    }
  }
}

provider "kubernetes" {
  config_path = var.kubeconfig_path
}
