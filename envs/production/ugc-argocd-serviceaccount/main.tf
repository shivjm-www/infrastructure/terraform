module "cluster_serviceaccount" {
  source = "../../../common/cluster-serviceaccount"

  sa_namespace = var.argo_cd_sa_namespace
  sa_username  = var.argo_cd_sa_username
}
