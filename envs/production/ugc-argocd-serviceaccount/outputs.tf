output "argo_cd_service_account_token" {
  sensitive = true
  value     = module.cluster_serviceaccount.service_account_token
}

output "ugc_ca_certificate" {
  sensitive = true
  value     = module.cluster_serviceaccount.ca_certificate
}
