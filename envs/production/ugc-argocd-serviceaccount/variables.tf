variable "kubeconfig_path" {
  type    = string
  default = "../ugc-kubeconfig"
}

variable "argo_cd_sa_username" {
  type    = string
  default = "argocd-remote"
}

variable "argo_cd_sa_namespace" {
  type    = string
  default = "argocd"
}
