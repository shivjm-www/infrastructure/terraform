output "bastion_ip" {
  value = module.bastion.bastion_ip
}

output "bastion_port" {
  value = module.bastion.bastion_port
}
