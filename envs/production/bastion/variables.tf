variable "do_token" {
  type = string
}

variable "do_region" {
  type = string
}

variable "tags" {
  type        = list(string)
  default     = ["website-tf", "bastion"]
  description = "One or more tags to apply to the cluster. The first tag in the list will be used in a firewall rule."
}

variable "do_project_id" {
  type = string
}

variable "node_name" {
  type = string
}

variable "node_image" {
  type        = string
  default     = "debian-11-x64"
  description = "The image or snapshot to create the database node from."
}

variable "node_size" {
  type        = string
  description = "The node size (see <https://docs.digitalocean.com/reference/api/api-reference/#tag/Sizes>)."
  default     = "g-2vcpu-8gb"
}

variable "main_private_network_uuid" {
  type        = string
  description = "The identifier of the network to place the database within."
}

variable "ssh_port" {
  type    = number
  default = 22
}

variable "ssh_key_name" {
  type = string
}

variable "ssh_key_file" {
  type = string
}

variable "firewall_rule_name" {
  type    = string
  default = "bastion-host"
}
