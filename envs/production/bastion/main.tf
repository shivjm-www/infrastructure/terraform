module "bastion" {
  source = "../../../common/bastion"

  do_region            = var.do_region
  tags                 = var.tags
  do_project_id        = var.do_project_id
  node_name            = var.node_name
  node_image           = var.node_image
  node_size            = var.node_size
  main_private_network_uuid = var.main_private_network_uuid
  ssh_port             = var.ssh_port
  ssh_key_name         = var.ssh_key_name
  ssh_key_file         = var.ssh_key_file
  firewall_rule_name   = var.firewall_rule_name
}
