module "discourse_bucket_alias" {
  source = "../../../common/cloudflare-cdn-alias"

  cloudflare_zone_id = var.cloudflare_zone_id
  cloudflare_zone    = var.cloudflare_zone
  record_name        = var.record_name
  new_subdomain      = var.new_subdomain
  target_domain      = var.target_domain
  bucket_name        = var.discourse_uploads_bucket_name
}
