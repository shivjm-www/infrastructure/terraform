variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}
variable "cloudflare_zone_id" {
  type      = string
  sensitive = true
}

variable "cloudflare_zone" {
  type        = string
  description = "Used for matching URLs."
  default     = "sjm.social"
}

variable "record_name" {
  type = string
}

variable "new_subdomain" {
  type      = string
  sensitive = true
}

variable "target_domain" {
  type      = string
  sensitive = true
}

variable "discourse_uploads_bucket_name" {
  type    = string
  default = "sjm-social--discourse-uploads"
}
