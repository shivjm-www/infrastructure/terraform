variable "do_token" {
  type = string
}

variable "do_region" {
  type = string
}

variable "do_tags" {
  type    = list(string)
  default = ["website-tf"]
}

variable "do_project_id" {
  type = string
}

variable "main_private_network_uuid" {
  type = string
}

variable "main_cluster_name" {
  type    = string
  default = "website-tf"
}

variable "kubernetes_version" {
  type        = string
  description = "Version of Kubernetes. Use `doctl kubernetes options versions` to see the full list."
  default     = "1.28.14-do.3"
}

variable "main_node_size" {
  type    = string
  default = "s-4vcpu-8gb"
}

variable "main_node_count" {
  type    = number
  default = 2
}

variable "ci_node_size" {
  type    = string
  default = "m-4vcpu-32gb"
}

variable "ci_node_count" {
  type    = number
  default = 0
}

variable "main_kubeconfig_filename" {
  type    = string
  default = "../kubeconfig"
}

variable "ugc_private_network_uuid" {
  type = string
}

variable "ugc_do_region" {
  type = string
}

variable "ugc_cluster_name" {
  type    = string
  default = "website-tf-ugc"
}

variable "ugc_node_size" {
  type    = string
  default = "s-2vcpu-4gb"
}

variable "ugc_node_count" {
  type    = number
  default = 2
}

variable "ugc_kubeconfig_filename" {
  type    = string
  default = "../ugc-kubeconfig"
}
