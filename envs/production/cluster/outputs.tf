output "main_kubeconfig" {
  value = module.cluster.main_kubeconfig
}

output "ugc_kubeconfig" {
  value = module.cluster.ugc_kubeconfig
}
