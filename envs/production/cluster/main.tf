module "cluster" {
  source = "../../../common/remote-cluster"

  do_region                 = var.do_region
  do_tags                   = var.do_tags
  do_project_id             = var.do_project_id
  main_private_network_uuid = var.main_private_network_uuid
  main_cluster_name         = var.main_cluster_name
  kubernetes_version        = var.kubernetes_version
  main_node_count           = var.main_node_count
  main_node_size            = var.main_node_size
  ci_node_count             = var.ci_node_count
  ci_node_size              = var.ci_node_size
  main_kubeconfig_filename  = var.main_kubeconfig_filename
  ugc_private_network_uuid  = var.ugc_private_network_uuid
  ugc_cluster_name          = var.ugc_cluster_name
  ugc_do_region             = var.ugc_do_region
  ugc_node_size             = var.ugc_node_size
  ugc_node_count            = var.ugc_node_count
  ugc_kubeconfig_filename   = var.ugc_kubeconfig_filename
}
