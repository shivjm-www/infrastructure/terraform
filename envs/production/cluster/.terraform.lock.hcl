# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.21.0"
  constraints = "2.21.0"
  hashes = [
    "h1:5/rQNY4d9WvCI+flyEJuUei+rOB53R8UR8GnXslUgTY=",
    "zh:122a4e7aa7315be52c484fa9b8e8681a42ad17e1f892ca17d6aeb575902df078",
    "zh:192f9428d965d1a5d2629e6ec1c238c6815601fbc909f905bf94ff9359ba88f6",
    "zh:20d88900cd24e9c05e2addeeb9fd5df285fba928d5f9981128352a6089fcec88",
    "zh:30eb650c04e4c6ab0b06549a345dacdfaf5597f24e916ed82cad798a546783e6",
    "zh:33864a8bc3f02507397d36bc5bf21ad1be11c188d2d0d45fe19063d0f949ff49",
    "zh:34431ea6fe25e734ed851595f622ff848c325fbf2f799d85a6cfcadedc1e9063",
    "zh:4be9ee1d632352504541d3629ce583e2b89ab9e5eb6a9d6547d37eb1c02dc341",
    "zh:723436531aaec6a7d651b15857194993baee6e59d7b1b8c22176ee44065de905",
    "zh:7305edf6305f3c0ba945509e679655d2b7c082f21723e2bd415c8f1f161a1a97",
    "zh:acd2124cb1c604351f9482d47ab9a16a1887b8d9df049c8da0d0e3791326404d",
    "zh:af6745bb9096a52e2570a758b55fd0a746b70d4bb04ba09ee33ab7dd63e87337",
    "zh:b676c58bbfd08bd3960ed78740a23ed17d959ac0348cab08ad34aaeaff952c7d",
    "zh:bfa739ac5fea3f89d9c403be91179729490370ed19b735070c9659e9265718aa",
    "zh:c1d5a0d0995fddf0df03f1da49de9531ab45138cf0b24e67c45f23a38ba91c33",
    "zh:cac52e76829a1fc01e165c4a66854b94b2813a21d392f6bddfacbba7edfd4f77",
    "zh:d55cdd70b89e7e9cd674c0f39cdef93dd0ace1d361bbae65583df9defb7d4725",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.5.2"
  hashes = [
    "h1:6NIiHWMbE9bFZaUiqC+OokdWSbW7g3+yQYnO4yvgtuY=",
    "zh:136299545178ce281c56f36965bf91c35407c11897f7082b3b983d86cb79b511",
    "zh:3b4486858aa9cb8163378722b642c57c529b6c64bfbfc9461d940a84cd66ebea",
    "zh:4855ee628ead847741aa4f4fc9bed50cfdbf197f2912775dd9fe7bc43fa077c0",
    "zh:4b8cd2583d1edcac4011caafe8afb7a95e8110a607a1d5fb87d921178074a69b",
    "zh:52084ddaff8c8cd3f9e7bcb7ce4dc1eab00602912c96da43c29b4762dc376038",
    "zh:71562d330d3f92d79b2952ffdda0dad167e952e46200c767dd30c6af8d7c0ed3",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:805f81ade06ff68fa8b908d31892eaed5c180ae031c77ad35f82cb7a74b97cf4",
    "zh:8b6b3ebeaaa8e38dd04e56996abe80db9be6f4c1df75ac3cccc77642899bd464",
    "zh:ad07750576b99248037b897de71113cc19b1a8d0bc235eb99173cc83d0de3b1b",
    "zh:b9f1c3bfadb74068f5c205292badb0661e17ac05eb23bfe8bd809691e4583d0e",
    "zh:cc4cbcd67414fefb111c1bf7ab0bc4beb8c0b553d01719ad17de9a047adff4d1",
  ]
}
