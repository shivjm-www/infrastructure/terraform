variable "b2_key_id" {
  type      = string
  sensitive = true
}

variable "b2_application_key" {
  type      = string
  sensitive = true
}

variable "discourse_uploads_bucket_name" {
  type    = string
  default = "sjm-social--discourse-uploads"
}

variable "discourse_uploads_delete_hidden_files_after" {
  type        = number
  description = "Number of days between a `DELETE` operation and when the file goes from hidden to deleted."
  default     = 30
}
