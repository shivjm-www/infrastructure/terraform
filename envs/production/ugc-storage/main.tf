module "storage" {
  source = "../../../common/ugc-storage"

  discourse_uploads_bucket_name               = var.discourse_uploads_bucket_name
  discourse_uploads_delete_hidden_files_after = var.discourse_uploads_delete_hidden_files_after
}
