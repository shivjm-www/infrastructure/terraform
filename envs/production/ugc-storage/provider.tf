terraform {
  backend "http" {
  }

  required_providers {
    b2 = {
      source  = "Backblaze/b2"
      version = "0.8.4"
    }
  }
}

provider "b2" {
  application_key_id = var.b2_key_id
  application_key    = var.b2_application_key
}
