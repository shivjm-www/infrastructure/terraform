variable "do_token" {
  type = string
}

variable "do_region" {
  type = string
}

variable "do_project_id" {
  type = string
}

variable "do_spaces_access_id" {
  type = string
}

variable "do_spaces_secret_key" {
  type = string
}

variable "loki_bucket_name" {
  type = string
}

variable "loki_bucket_expiration" {
  type        = number
  description = "Number of days after which to expire items."
}

variable "mimir_bucket_name" {
  type = string
}

variable "tempo_bucket_name" {
  type = string
}
