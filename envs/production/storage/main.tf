module "storage" {
  source = "../../../common/storage"

  do_region              = var.do_region
  do_project_id          = var.do_project_id
  loki_bucket_name       = var.loki_bucket_name
  loki_bucket_expiration = var.loki_bucket_expiration
  mimir_bucket_name      = var.mimir_bucket_name
  tempo_bucket_name      = var.tempo_bucket_name
}
