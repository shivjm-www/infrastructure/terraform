variable "do_token" {
  type = string
}

variable "do_region" {
  type = string
}

variable "main_vpc_name" {
  type        = string
  default     = "website-tf"
  description = "The name to give the VPC."
}

variable "main_vpc_ip_range" {
  type        = string
  description = "The IP range to use within the VPC."
  default     = "10.10.10.0/24"
}

variable "do_ugc_region" {
  type = string
}

variable "ugc_vpc_name" {
  type        = string
  default     = "website-tf-ugc"
  description = "The name to give the VPC."
}

variable "ugc_vpc_ip_range" {
  type        = string
  description = "The IP range to use within the VPC."
  default     = "10.10.11.0/24"
}

variable "do_project_name" {
  type    = string
  default = "shivjm-apps"
}

variable "do_project_description" {
  type    = string
  default = "Website infrastructure, created with Terraform."
}

variable "do_project_purpose" {
  default = "Web Application"
}

variable "gitlab_pat" {
  type      = string
  sensitive = true
}

variable "argocd_webhook_url" {
  type    = string
  default = "https://argo.internal.shivjm.in/api/webhook"
}
