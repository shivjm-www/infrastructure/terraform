resource "digitalocean_vpc" "website" {
  name     = var.main_vpc_name
  region   = var.do_region
  ip_range = var.main_vpc_ip_range
}

resource "digitalocean_vpc" "website_ugc" {
  name     = var.ugc_vpc_name
  region   = var.do_ugc_region
  ip_range = var.ugc_vpc_ip_range
}

resource "digitalocean_project" "shared_project" {
  name        = var.do_project_name
  description = var.do_project_description
  purpose     = var.do_project_purpose
}

resource "random_string" "superuser_name" {
  length = 20

  # only use lowercase letters
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "superuser_password" {
  length = 20
}

resource "random_string" "promscale_username" {
  length = 20

  # only use lowercase letters
  upper   = false
  special = false
  numeric = false
}

resource "random_password" "promscale" {
  length = 20
}

resource "random_password" "argocd_webhook_secret" {
  length = 24
}

module "gitlab_access" {
  source = "../../../common/gitlab-access"

  argocd_webhook_url    = var.argocd_webhook_url
  argocd_webhook_secret = random_password.argocd_webhook_secret.result
}
