output "main_vpc_id" {
  value     = digitalocean_vpc.website.id
  sensitive = true
}

output "ugc_vpc_id" {
  value     = digitalocean_vpc.website_ugc.id
  sensitive = true
}

output "project_id" {
  value     = digitalocean_project.shared_project.id
  sensitive = true
}

output "gitlab_argocd_token" {
  value     = module.gitlab_access.argocd_token
  sensitive = true
}

output "gitlab_runner_token" {
  value     = module.gitlab_access.runners_token
  sensitive = true
}

output "argocd_webhook_secret" {
  value     = random_password.argocd_webhook_secret.result
  sensitive = true
}
