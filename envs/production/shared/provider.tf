terraform {
  backend "http" {
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.21.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.18.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "4.0.3"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "gitlab" {
  token = var.gitlab_pat
}
