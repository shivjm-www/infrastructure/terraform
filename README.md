# Terraform stack

This is the Terraform stack for my personal DigitalOcean Kubernetes
cluster, now only somewhat in flux. I’m sharing it here with the
caveat that I’m not an expert (quite the contrary) and it isn’t a
readymade solution for anyone (least of all me). This is the expected
order:

1. Shared resources (VPC, project, random credentials and
   identifiers).
2. Storage (Spaces).
3. Database (PostgreSQL with TimescaleDB & Promscale available).
4. Kubernetes cluster.
5. Argo CD deployment.

Once Argo CD has been deployed, it takes over installing applications
in the cluster.

## Layout

The repository generally follows the Terraservices pattern, but since
this is just a personal project, inter-module dependencies use
relative paths, not versioned repository URLs.

* Common modules go under `common/`
* Environments (which assemble the common modules into usable
  components) go under `envs/`
* Everything is repeated roughly two hundred times because we do
  Terraform here

## Usage

Put rarely-changed variables for individual modules in
terraform.tfvars in the appropriate directory (e.g.
`envs/production/db`). Set environment variables for other data. Use
the `TF_VAR_` prefix for automatic propagation:

```powershell
$env:TF_VAR_private_network_uuid = $(tf '-chdir=../shared' output -raw vpc_id)
tf apply
```

After creating the cluster, set the `KUBECONFIG` environment variable
to the file created by the module.

## Development

Install [pre-commit](https://pre-commit.com/) and run <kbd>pre-commit
install</kbd>.

### Running locally

```
kind create cluster
pushd envs/local
kind export kubeconfig --kubeconfig ./kubeconfig.local
```

```
pushd mocks
tf init
tf plan -out plan.tfplan
tf apply plan.tfplan
popd
```

### PowerShell helpers

TODO.

```powershell
  $vars = @(
      @("tsdb_cluster_host", "db", "host"),
      @("tsdb_cluster_port", "db", "port"),
      @("tsdb_superuser_name", "shared", "superuser_name"),
      @("tsdb_promscale_username", "shared", "promscale_username"),
      @("tsdb_promscale_password", "shared", "promscale_password"),
      @("gitlab_password", "shared", "gitlab_argocd_token"),
      @("do_project_id", "shared", "project_id"),
      @("private_network_uuid", "shared", "vpc_id"),
  )
```
